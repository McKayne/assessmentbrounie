# AssessmentBrounie

#### Description
Tarea prueba para la empresa Brounie hecho por Nikolay Taran

#### Instructions

Para LogicAssessment por favor usa:

1. kotlinc brounie.kt -include-runtime -d brounie.jar && java -jar brounie.jar (Kotlin)
2. (Opcional) python3 LogicAssessment.py (Python)

Database Assessment está disponible en documento BrounieDataBaseAssessment.pdf
