import java.math.BigInteger

fun main() {
	task1()
	task2()
	task3()
	task4()
}

private fun task1() {
	println("Given an integer n, return the largest number that contains exactly n digits.")

	// User inputs N from the keyboard
	print("Please enter N: ")
	val inputN = readLine() // Example: 123

	// We need to check at first if input is an actual integer
	val n = inputN?.toIntOrNull() // If input is number we convert it to an integer
	if (n is Int) {
		// And after that we check that N is strictly more than zero
		if (n > 0) {
			val stringBuilder = StringBuilder()
			for (i in 0 until n) {
				stringBuilder.append("9")
			}

			val nString = stringBuilder.toString()
			val nInteger = BigInteger(nString) // Ready largest number containing N digits
			println(nInteger)
		} else {
			println("Oops, the numbers containing zero or negative quantity of digits don't exist in this world")
		}
	} else {
		println("Looks like the input is not a single number")
	}
}

private fun task2() {
	println("Write a function to check whether an input string is a valid IPv4")

	// User inputs string from the keyboard
	print("Please enter input string: ")
	val inputStr = readLine() // Example: 192.168.1.45

	// Correct IPv4 must contain strictly 4 numbers divided by dots
	val parts = inputStr?.split(".")
	if (parts is List<String> && parts.size == 4) {
		// Attempting to convert each of 4 parts of an supposed IPv4 to a number
		run breaking@ {
			parts.forEach {
				val part = it.toIntOrNull()
				// Each number of valid IPv4 parts must be from 0 to 255
				if (!(part is Int && part in 0..255)) {
					println("This is NOT valid IPv4")
					return@breaking
				}
			}

			println("This is valid IPv4")
		}
	} else {
		println("This is NOT valid IPv4")
	}
}

private fun task3() {
	println("You are given a two-digit integer n . Return the sum of its digits.")

	// User inputs N from the keyboard
	print("Please enter N: ")
	val inputN = readLine() // Example: 69

	// We need to check at first if input is an actual integer
	val n = inputN?.toIntOrNull() // If input is number we convert it to an integer
	if (n is Int) {
		// Now checking for N to have strictly two digits. Negative numbers such as -13 count as two digit too because "-" sign is not a digit
		if (Math.abs(n) in 10..99) {
			val digit1 = "${Math.abs(n).toString()[0]}".toInt()
			val digit2 = "${Math.abs(n).toString()[1]}".toInt()

			val digitsSum = digit1 + digit2 // Ready sum of the digits
			println(digitsSum)
		} else {
			println("This is not a two-digit integer")
		}
	} else {
		println("Looks like the input is not a single number")
	}
}

private fun task4() {
	println("n children have got m pieces of candy. They want to eat as much candy as they can, but each child must eat exactly the same amount of candy as any other child. Determine how many pieces of candy will be eaten by all the children together. Individual pieces of candy cannot be split.")

	// User inputs N from the keyboard
	print("Please enter number of children N: ")
	val inputN = readLine() // Example: 5

	// User inputs M from the keyboard
	print("Please enter number of pieces of candy M: ")
	val inputM = readLine() // Example: 12

	// We need to check at first if both inputs are actual integers
	val n = inputN?.toIntOrNull()
	val m = inputM?.toIntOrNull()
	if (n is Int && m is Int) {
		// And after that we check that N and M are strictly non-negative. Zeros are allowed here too because there is not mentioned in the task that number of children and candies must be strictly more than zero
		if (n >= 0 && m >= 0) {
			var candiesEaten: Int
			n.let {
				when {
					it == 0 -> {
						candiesEaten = 0 // If there are zero children then obviously no candies can be eaten
					}
					it > m -> {
						candiesEaten = 0 // If number of candies are less than number of children no candies can be eaten too because it would be situation in which some of the children ate candies but other children don't, but it would be a violation of the task requirement
					}
					else -> {
						val candiesEatenByChild = m / it
						candiesEaten = candiesEatenByChild * it // Ready number of eaten candies
					}
				}
			}

			println("$n children ate $candiesEaten candies out of $m")
		} else {
			println("N and M cannot be negative numbers")
		}
	} else {
		println("Looks like N and/or M are not numbers")
	}
}