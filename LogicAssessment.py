def task_1():
	print('Given an integer n, return the largest number that contains exactly n digits.')

	# User inputs N from the keyboard
	input_n = input('Please enter N: ') # Example: 123

	if input_n.lstrip('-').isdigit(): # We need to check at first if input is an actual integer
		try:
			n = int(input_n) # If input is number we convert it to an integer

			if n > 0: # And after that we check that N is strictly more than zero
				n_string = ''
				for i in range(0, n):
					n_string = n_string + '9'

				n_integer = int(n_string) # Ready largest number containing N digits
				print(n_integer)
			else:
				print("Oops, the numbers containing zero or negative quantity of digits don't exist in this world")
		except ValueError:
			print('Looks like the input is not a single number')
	else:
		print('Looks like the input is not a single number')

def task_2():
	print('Write a function to check whether an input string is a valid IPv4')

	# User inputs string from the keyboard
	input_str = input('Please enter input string: ') # Example: 192.168.1.45

	parts = input_str.split('.') # Correct IPv4 must contain strictly 4 numbers divided by dots
	if len(parts) == 4:
		try:
			# Attempting to convert each of 4 parts of an supposed IPv4 to a number
			parts_array = [int(parts[0]), int(parts[1]), int(parts[2]), int(parts[3])]
			if min(parts_array) >= 0 and max(parts_array) < 256: # each number of valid IPv4 parts must be from 0 to 255
				print('This is valid IPv4')
			else:
				print('This is NOT valid IPv4')
		except ValueError:
			print('This is NOT valid IPv4')
	else:
		print('This is NOT valid IPv4')

def task_3():
	print('You are given a two-digit integer n . Return the sum of its digits.')

	# User inputs N from the keyboard
	input_n = input('Please enter N: ') # Example: 69

	if input_n.lstrip('-').isdigit(): # We need to check at first if input is an actual integer
		try:
			n = int(input_n) # If input is number we convert it to an integer

			if 9 < abs(n) < 100: # Now checking for N to have strictly two digits. Negative numbers such as -13 count as two digit too because "-" sign is not a digit
				digit_1 = int(str(abs(n))[0])
				digit_2 = int(str(abs(n))[1])

				digits_sum = digit_1 + digit_2 # Ready sum of the digits
				print(digits_sum)
			else:
				print('This is not a two-digit integer')
		except ValueError:
			print('Looks like the input is not a single number')
	else:
		print('Looks like the input is not a single number')

def task_4():
	print('n children have got m pieces of candy. They want to eat as much candy as they can, but each child must eat exactly the same amount of candy as any other child. Determine how many pieces of candy will be eaten by all the children together. Individual pieces of candy cannot be split.')

	# User inputs N from the keyboard
	input_n = input('Please enter number of children N: ') # Example: 5

	# User inputs M from the keyboard
	input_m = input('Please enter number of pieces of candy M: ') # Example: 12

	if input_n.lstrip('-').isdigit() and input_m.lstrip('-').isdigit(): # We need to check at first if both inputs are actual integers
		try:
			# If inputs are numbers we convert them to integers
			n = int(input_n)
			m = int(input_m)

			if n >= 0 and m >= 0: # And after that we check that N and M are strictly non-negative. Zeros are allowed here too because there is not mentioned in the task that number of children and candies must be strictly more than zero
				if n == 0: # If there are zero children then obviously no candies can be eaten
					candies_eaten = 0
				elif n > m: # If number of candies are less than number of children no candies can be eaten too because it would be situation in which some of the children ate candies but other children don't, but it would be a violation of the task requirement
					candies_eaten = 0
				else:
					candies_eaten_by_child = int(m / n)
					candies_eaten = candies_eaten_by_child * n # Ready number of eaten candies

				print(str(n) + ' children ate ' + str(candies_eaten) + ' candies out of ' + str(m))
			else:
				print('N and/or M cannot be negative numbers')
		except ValueError:
			print('Looks like N and/or M are not numbers')
	else:
		print('Looks like N and/or M are not numbers')

task_1()
task_2()
task_3()
task_4()